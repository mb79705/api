//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
//Con estas cabeceras no es necesario instalar la extensión CORS Everywhere a FireFox
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Header", "Origin, X-Request-With, Contect-Type, Accept");
  next();
});

var requestjson = require('request-json');
var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/bdbanca4mb79705/collections/movimientos?apiKey=bBufAh3JB23NMIlFkr-VtvDvGEbxfc2x";
var clienteMLab = requestjson.createClient(urlmovimientosMLab);

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res) {
  //res.send('Hola mundo');
  res.sendFile(path.join(__dirname, 'index.html'));
});
/* Para pasar parametros se utilizan : y la variable con el valor (:idcliente)*/
/* Prueba para commit */
/* Y obtenemos el valor consultado con req.params */
app.get("/clientes/:idcliente", function(req, res) {
  //res.send('Hola mundo');
  res.send('Aquí tiene al cliente numero: ' + req.params.idcliente);
});

app.post("/", function(req, res) {
  res.send('Hemos recibido su petición POST cambiada');
});

app.put("/", function(req, res) {
  res.send('Hemos recibido su petición PUT');
});

app.delete("/", function(req, res) {
  res.send('Hemos recibido su petición DELETE');
});

app.get("/movimientos", function(req, res) {
  clienteMLab.get('',  function(err, resM, body){
    if(err){
      console.log(body);
    } else {
      res.send(body);
    }
  });
});

app.post("/movimientos", function(req, res) {
  clienteMLab.post('', req.body, function(err, resM, body) {
      res.send(body);
  });
});
